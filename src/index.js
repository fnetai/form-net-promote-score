import React from 'react';
import Layout from "@fnet/react-layout-asya";

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Slider from '@mui/material/Slider';

function valueLabelFormat(value) {
  return `${value}%`;
}

export default (props) => {

  const [inputValue] = React.useState(props.input?.value || 50);
  const [inputStep] = React.useState(props.input?.step || 10);

  return (
    <Layout {...props}>

      <Slider
        marks
        defaultValue={inputValue}
        step={inputStep}
        valueLabelDisplay="on"
        valueLabelFormat={valueLabelFormat}
      />

      <Box sx={{ display: "flex", width: "100%", justifyContent: "space-between" }}>
        <Typography>Not likely at all</Typography>
        <Typography>Extremely likely</Typography>
      </Box>
    </Layout>
  );
}