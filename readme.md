# @fnet/form-net-promote-score

## Introduction

The `@fnet/form-net-promote-score` component is a simple, user-friendly React component focused on providing a visual representation and interaction mechanism for users to express opinions in a percentage format. Its primary purpose is to allow users to indicate their likelihood or inclination towards a particular option or statement using a slider interface.

## How It Works

This component integrates into your existing React application seamlessly. It uses a slider to capture user input with steps and allows for customizable start values. As users adjust the slider, the percentage value they select is displayed clearly above the slider for instant feedback. 

## Key Features

- **Interactive Slider:** Users can quickly and easily set preferences or opinions in percentage form.
- **Default Value and Step Control:** Pre-set starting point and incremental steps can be tailored as per the requirement.
- **End Labels:** Clearly marked labels indicate the extent of the scale, ranging from "Not likely at all" to "Extremely likely".
- **Visual Feedback:** Displays the selected percentage value above the slider to keep users informed about their choice.

## Conclusion

The `@fnet/form-net-promote-score` component offers a straightforward and useful tool for collecting user preference data in a visually engaging manner. Designed for ease of use, it lends itself well to applications that require user input on scales, such as feedback forms or surveys, without complicating the user experience.